/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbIntervSistSaneMelhor;

/**
 *
 * @author danielo
 */
@Local
public interface TbIntervSistSaneMelhorFacadeLocal {

    void create(TbIntervSistSaneMelhor tbIntervSistSaneMelhor);

    void edit(TbIntervSistSaneMelhor tbIntervSistSaneMelhor);

    void remove(TbIntervSistSaneMelhor tbIntervSistSaneMelhor);

    TbIntervSistSaneMelhor find(Object id);

    List<TbIntervSistSaneMelhor> findAll();

    List<TbIntervSistSaneMelhor> findRange(int[] range);

    int count();
    
}
