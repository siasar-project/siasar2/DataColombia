/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.DjangoSession;

/**
 *
 * @author danielo
 */
@Local
public interface DjangoSessionFacadeLocal {

    void create(DjangoSession djangoSession);

    void edit(DjangoSession djangoSession);

    void remove(DjangoSession djangoSession);

    DjangoSession find(Object id);

    List<DjangoSession> findAll();

    List<DjangoSession> findRange(int[] range);

    int count();
    
}
