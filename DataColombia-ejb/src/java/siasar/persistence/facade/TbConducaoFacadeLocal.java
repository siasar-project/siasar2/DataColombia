/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbConducao;

/**
 *
 * @author danielo
 */
@Local
public interface TbConducaoFacadeLocal {

    void create(TbConducao tbConducao);

    void edit(TbConducao tbConducao);

    void remove(TbConducao tbConducao);

    TbConducao find(Object id);

    List<TbConducao> findAll();

    List<TbConducao> findRange(int[] range);

    int count();
    
}
