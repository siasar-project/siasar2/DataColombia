/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbDirTecnico;

/**
 *
 * @author danielo
 */
@Local
public interface TbDirTecnicoFacadeLocal {

    void create(TbDirTecnico tbDirTecnico);

    void edit(TbDirTecnico tbDirTecnico);

    void remove(TbDirTecnico tbDirTecnico);

    TbDirTecnico find(Object id);

    List<TbDirTecnico> findAll();

    List<TbDirTecnico> findRange(int[] range);

    int count();
    
}
