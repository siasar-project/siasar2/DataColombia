/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthGroup;

/**
 *
 * @author danielo
 */
@Local
public interface AuthGroupFacadeLocal {

    void create(AuthGroup authGroup);

    void edit(AuthGroup authGroup);

    void remove(AuthGroup authGroup);

    AuthGroup find(Object id);

    List<AuthGroup> findAll();

    List<AuthGroup> findRange(int[] range);

    int count();
    
}
