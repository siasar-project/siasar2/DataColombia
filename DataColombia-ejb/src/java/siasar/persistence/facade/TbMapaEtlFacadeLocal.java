/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbMapaEtl;

/**
 *
 * @author danielo
 */
@Local
public interface TbMapaEtlFacadeLocal {

    void create(TbMapaEtl tbMapaEtl);

    void edit(TbMapaEtl tbMapaEtl);

    void remove(TbMapaEtl tbMapaEtl);

    TbMapaEtl find(Object id);

    List<TbMapaEtl> findAll();

    List<TbMapaEtl> findRange(int[] range);

    int count();
    
}
