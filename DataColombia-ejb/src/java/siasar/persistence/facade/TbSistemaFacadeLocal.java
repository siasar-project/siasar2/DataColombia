/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbSistema;

/**
 *
 * @author danielo
 */
@Local
public interface TbSistemaFacadeLocal {

    void create(TbSistema tbSistema);

    void edit(TbSistema tbSistema);

    void remove(TbSistema tbSistema);

    TbSistema find(Object id);

    List<TbSistema> findAll();

    List<TbSistema> findRange(int[] range);

    int count();
    
}
