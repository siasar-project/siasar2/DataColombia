/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.DjangoMigrations;

/**
 *
 * @author danielo
 */
@Local
public interface DjangoMigrationsFacadeLocal {

    void create(DjangoMigrations djangoMigrations);

    void edit(DjangoMigrations djangoMigrations);

    void remove(DjangoMigrations djangoMigrations);

    DjangoMigrations find(Object id);

    List<DjangoMigrations> findAll();

    List<DjangoMigrations> findRange(int[] range);

    int count();
    
}
