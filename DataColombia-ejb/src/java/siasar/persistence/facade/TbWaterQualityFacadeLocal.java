/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbWaterQuality;

/**
 *
 * @author danielo
 */
@Local
public interface TbWaterQualityFacadeLocal {

    void create(TbWaterQuality tbWaterQuality);

    void edit(TbWaterQuality tbWaterQuality);

    void remove(TbWaterQuality tbWaterQuality);

    TbWaterQuality find(Object id);

    List<TbWaterQuality> findAll();

    List<TbWaterQuality> findRange(int[] range);

    int count();
    
}
