/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.ExplorerQuerylog;

/**
 *
 * @author danielo
 */
@Local
public interface ExplorerQuerylogFacadeLocal {

    void create(ExplorerQuerylog explorerQuerylog);

    void edit(ExplorerQuerylog explorerQuerylog);

    void remove(ExplorerQuerylog explorerQuerylog);

    ExplorerQuerylog find(Object id);

    List<ExplorerQuerylog> findAll();

    List<ExplorerQuerylog> findRange(int[] range);

    int count();
    
}
