/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.TbMapaEtl;

/**
 *
 * @author danielo
 */
@Stateless
public class TbMapaEtlFacade extends AbstractFacade<TbMapaEtl> implements TbMapaEtlFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbMapaEtlFacade() {
        super(TbMapaEtl.class);
    }
    
}
