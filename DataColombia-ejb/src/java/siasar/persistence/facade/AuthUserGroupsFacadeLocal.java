/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthUserGroups;

/**
 *
 * @author danielo
 */
@Local
public interface AuthUserGroupsFacadeLocal {

    void create(AuthUserGroups authUserGroups);

    void edit(AuthUserGroups authUserGroups);

    void remove(AuthUserGroups authUserGroups);

    AuthUserGroups find(Object id);

    List<AuthUserGroups> findAll();

    List<AuthUserGroups> findRange(int[] range);

    int count();
    
}
