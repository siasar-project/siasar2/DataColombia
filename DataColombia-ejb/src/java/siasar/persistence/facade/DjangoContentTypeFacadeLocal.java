/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.DjangoContentType;

/**
 *
 * @author danielo
 */
@Local
public interface DjangoContentTypeFacadeLocal {

    void create(DjangoContentType djangoContentType);

    void edit(DjangoContentType djangoContentType);

    void remove(DjangoContentType djangoContentType);

    DjangoContentType find(Object id);

    List<DjangoContentType> findAll();

    List<DjangoContentType> findRange(int[] range);

    int count();
    
}
