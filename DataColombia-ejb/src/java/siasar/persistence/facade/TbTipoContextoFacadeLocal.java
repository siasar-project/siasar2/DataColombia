/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbTipoContexto;

/**
 *
 * @author danielo
 */
@Local
public interface TbTipoContextoFacadeLocal {

    void create(TbTipoContexto tbTipoContexto);

    void edit(TbTipoContexto tbTipoContexto);

    void remove(TbTipoContexto tbTipoContexto);

    TbTipoContexto find(Object id);

    List<TbTipoContexto> findAll();

    List<TbTipoContexto> findRange(int[] range);

    int count();
    
}
