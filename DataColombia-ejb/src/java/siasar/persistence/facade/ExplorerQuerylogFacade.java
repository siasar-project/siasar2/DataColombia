/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.ExplorerQuerylog;

/**
 *
 * @author danielo
 */
@Stateless
public class ExplorerQuerylogFacade extends AbstractFacade<ExplorerQuerylog> implements ExplorerQuerylogFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ExplorerQuerylogFacade() {
        super(ExplorerQuerylog.class);
    }
    
}
