/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.SpatialRefSys;

/**
 *
 * @author danielo
 */
@Local
public interface SpatialRefSysFacadeLocal {

    void create(SpatialRefSys spatialRefSys);

    void edit(SpatialRefSys spatialRefSys);

    void remove(SpatialRefSys spatialRefSys);

    SpatialRefSys find(Object id);

    List<SpatialRefSys> findAll();

    List<SpatialRefSys> findRange(int[] range);

    int count();
    
}
