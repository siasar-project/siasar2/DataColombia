/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.DjangoSite;

/**
 *
 * @author danielo
 */
@Local
public interface DjangoSiteFacadeLocal {

    void create(DjangoSite djangoSite);

    void edit(DjangoSite djangoSite);

    void remove(DjangoSite djangoSite);

    DjangoSite find(Object id);

    List<DjangoSite> findAll();

    List<DjangoSite> findRange(int[] range);

    int count();
    
}
