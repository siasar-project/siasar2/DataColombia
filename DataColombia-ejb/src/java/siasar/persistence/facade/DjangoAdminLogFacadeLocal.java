/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.DjangoAdminLog;

/**
 *
 * @author danielo
 */
@Local
public interface DjangoAdminLogFacadeLocal {

    void create(DjangoAdminLog djangoAdminLog);

    void edit(DjangoAdminLog djangoAdminLog);

    void remove(DjangoAdminLog djangoAdminLog);

    DjangoAdminLog find(Object id);

    List<DjangoAdminLog> findAll();

    List<DjangoAdminLog> findRange(int[] range);

    int count();
    
}
