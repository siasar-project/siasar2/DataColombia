/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbCaptacao;

/**
 *
 * @author danielo
 */
@Local
public interface TbCaptacaoFacadeLocal {

    void create(TbCaptacao tbCaptacao);

    void edit(TbCaptacao tbCaptacao);

    void remove(TbCaptacao tbCaptacao);

    TbCaptacao find(Object id);

    List<TbCaptacao> findAll();

    List<TbCaptacao> findRange(int[] range);

    int count();
    
}
