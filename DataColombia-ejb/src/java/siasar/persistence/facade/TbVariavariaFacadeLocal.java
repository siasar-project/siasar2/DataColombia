/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbVariavaria;

/**
 *
 * @author danielo
 */
@Local
public interface TbVariavariaFacadeLocal {

    void create(TbVariavaria tbVariavaria);

    void edit(TbVariavaria tbVariavaria);

    void remove(TbVariavaria tbVariavaria);

    TbVariavaria find(Object id);

    List<TbVariavaria> findAll();

    List<TbVariavaria> findRange(int[] range);

    int count();
    
}
