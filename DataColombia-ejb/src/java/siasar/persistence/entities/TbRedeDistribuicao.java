/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_rede_distribuicao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRedeDistribuicao.findAll", query = "SELECT t FROM TbRedeDistribuicao t")
    , @NamedQuery(name = "TbRedeDistribuicao.findByRedisSeq", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.redisSeq = :redisSeq")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF001001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF001001 = :sysF001001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF002001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF002001 = :sysF002001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF003001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF003001 = :sysF003001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF004001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF004001 = :sysF004001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF005001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF005001 = :sysF005001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF006001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF006001 = :sysF006001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF007001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF007001 = :sysF007001")
    , @NamedQuery(name = "TbRedeDistribuicao.findBySysF008001", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.sysF008001 = :sysF008001")
    , @NamedQuery(name = "TbRedeDistribuicao.findByDateLastUpdate", query = "SELECT t FROM TbRedeDistribuicao t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbRedeDistribuicao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "redis_seq")
    private Long redisSeq;
    @Size(max = 100)
    @Column(name = "sys_f_001_001")
    private String sysF001001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_f_002_001")
    private BigDecimal sysF002001;
    @Column(name = "sys_f_003_001")
    private Integer sysF003001;
    @Column(name = "sys_f_004_001")
    private Integer sysF004001;
    @Column(name = "sys_f_005_001")
    private Integer sysF005001;
    @Column(name = "sys_f_006_001")
    private Integer sysF006001;
    @Column(name = "sys_f_007_001")
    private Integer sysF007001;
    @Size(max = 2147483647)
    @Column(name = "sys_f_008_001")
    private String sysF008001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbRedeDistribuicao() {
    }

    public TbRedeDistribuicao(Long redisSeq) {
        this.redisSeq = redisSeq;
    }

    public Long getRedisSeq() {
        return redisSeq;
    }

    public void setRedisSeq(Long redisSeq) {
        this.redisSeq = redisSeq;
    }

    public String getSysF001001() {
        return sysF001001;
    }

    public void setSysF001001(String sysF001001) {
        this.sysF001001 = sysF001001;
    }

    public BigDecimal getSysF002001() {
        return sysF002001;
    }

    public void setSysF002001(BigDecimal sysF002001) {
        this.sysF002001 = sysF002001;
    }

    public Integer getSysF003001() {
        return sysF003001;
    }

    public void setSysF003001(Integer sysF003001) {
        this.sysF003001 = sysF003001;
    }

    public Integer getSysF004001() {
        return sysF004001;
    }

    public void setSysF004001(Integer sysF004001) {
        this.sysF004001 = sysF004001;
    }

    public Integer getSysF005001() {
        return sysF005001;
    }

    public void setSysF005001(Integer sysF005001) {
        this.sysF005001 = sysF005001;
    }

    public Integer getSysF006001() {
        return sysF006001;
    }

    public void setSysF006001(Integer sysF006001) {
        this.sysF006001 = sysF006001;
    }

    public Integer getSysF007001() {
        return sysF007001;
    }

    public void setSysF007001(Integer sysF007001) {
        this.sysF007001 = sysF007001;
    }

    public String getSysF008001() {
        return sysF008001;
    }

    public void setSysF008001(String sysF008001) {
        this.sysF008001 = sysF008001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (redisSeq != null ? redisSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRedeDistribuicao)) {
            return false;
        }
        TbRedeDistribuicao other = (TbRedeDistribuicao) object;
        if ((this.redisSeq == null && other.redisSeq != null) || (this.redisSeq != null && !this.redisSeq.equals(other.redisSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbRedeDistribuicao[ redisSeq=" + redisSeq + " ]";
    }
    
}
