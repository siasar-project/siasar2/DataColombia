/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_variavel_dw")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVariavelDw.findAll", query = "SELECT t FROM TbVariavelDw t")
    , @NamedQuery(name = "TbVariavelDw.findByVardwSeq", query = "SELECT t FROM TbVariavelDw t WHERE t.vardwSeq = :vardwSeq")
    , @NamedQuery(name = "TbVariavelDw.findByVardwValor", query = "SELECT t FROM TbVariavelDw t WHERE t.vardwValor = :vardwValor")
    , @NamedQuery(name = "TbVariavelDw.findByVardwEntidade", query = "SELECT t FROM TbVariavelDw t WHERE t.vardwEntidade = :vardwEntidade")
    , @NamedQuery(name = "TbVariavelDw.findByVardwTempo", query = "SELECT t FROM TbVariavelDw t WHERE t.vardwTempo = :vardwTempo")
    , @NamedQuery(name = "TbVariavelDw.findByVardwErro", query = "SELECT t FROM TbVariavelDw t WHERE t.vardwErro = :vardwErro")
    , @NamedQuery(name = "TbVariavelDw.findByDataCalculo", query = "SELECT t FROM TbVariavelDw t WHERE t.dataCalculo = :dataCalculo")})
public class TbVariavelDw implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vardw_seq")
    private Long vardwSeq;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vardw_valor")
    private Float vardwValor;
    @Column(name = "vardw_entidade")
    private BigInteger vardwEntidade;
    @Column(name = "vardw_tempo")
    @Temporal(TemporalType.DATE)
    private Date vardwTempo;
    @Size(max = 2147483647)
    @Column(name = "vardw_erro")
    private String vardwErro;
    @Column(name = "data_calculo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCalculo;
    @JoinColumn(name = "vardw_localidade", referencedColumnName = "tda_seq")
    @ManyToOne
    private TbDivisaoAdmin vardwLocalidade;
    @JoinColumn(name = "tipo_con_seq", referencedColumnName = "tipo_con_seq")
    @ManyToOne
    private TbTipoContexto tipoConSeq;
    @JoinColumn(name = "varia_seq", referencedColumnName = "varia_seq")
    @ManyToOne
    private TbVariavel variaSeq;

    public TbVariavelDw() {
    }

    public TbVariavelDw(Long vardwSeq) {
        this.vardwSeq = vardwSeq;
    }

    public Long getVardwSeq() {
        return vardwSeq;
    }

    public void setVardwSeq(Long vardwSeq) {
        this.vardwSeq = vardwSeq;
    }

    public Float getVardwValor() {
        return vardwValor;
    }

    public void setVardwValor(Float vardwValor) {
        this.vardwValor = vardwValor;
    }

    public BigInteger getVardwEntidade() {
        return vardwEntidade;
    }

    public void setVardwEntidade(BigInteger vardwEntidade) {
        this.vardwEntidade = vardwEntidade;
    }

    public Date getVardwTempo() {
        return vardwTempo;
    }

    public void setVardwTempo(Date vardwTempo) {
        this.vardwTempo = vardwTempo;
    }

    public String getVardwErro() {
        return vardwErro;
    }

    public void setVardwErro(String vardwErro) {
        this.vardwErro = vardwErro;
    }

    public Date getDataCalculo() {
        return dataCalculo;
    }

    public void setDataCalculo(Date dataCalculo) {
        this.dataCalculo = dataCalculo;
    }

    public TbDivisaoAdmin getVardwLocalidade() {
        return vardwLocalidade;
    }

    public void setVardwLocalidade(TbDivisaoAdmin vardwLocalidade) {
        this.vardwLocalidade = vardwLocalidade;
    }

    public TbTipoContexto getTipoConSeq() {
        return tipoConSeq;
    }

    public void setTipoConSeq(TbTipoContexto tipoConSeq) {
        this.tipoConSeq = tipoConSeq;
    }

    public TbVariavel getVariaSeq() {
        return variaSeq;
    }

    public void setVariaSeq(TbVariavel variaSeq) {
        this.variaSeq = variaSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vardwSeq != null ? vardwSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVariavelDw)) {
            return false;
        }
        TbVariavelDw other = (TbVariavelDw) object;
        if ((this.vardwSeq == null && other.vardwSeq != null) || (this.vardwSeq != null && !this.vardwSeq.equals(other.vardwSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbVariavelDw[ vardwSeq=" + vardwSeq + " ]";
    }
    
}
