/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_tipo_contexto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTipoContexto.findAll", query = "SELECT t FROM TbTipoContexto t")
    , @NamedQuery(name = "TbTipoContexto.findByTipoConSeq", query = "SELECT t FROM TbTipoContexto t WHERE t.tipoConSeq = :tipoConSeq")
    , @NamedQuery(name = "TbTipoContexto.findByTipoConNome", query = "SELECT t FROM TbTipoContexto t WHERE t.tipoConNome = :tipoConNome")
    , @NamedQuery(name = "TbTipoContexto.findByTipoConCodigo", query = "SELECT t FROM TbTipoContexto t WHERE t.tipoConCodigo = :tipoConCodigo")})
public class TbTipoContexto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipo_con_seq")
    private Integer tipoConSeq;
    @Size(max = 255)
    @Column(name = "tipo_con_nome")
    private String tipoConNome;
    @Size(max = 10)
    @Column(name = "tipo_con_codigo")
    private String tipoConCodigo;
    @OneToMany(mappedBy = "tipoConSeqPai")
    private List<TbTipoContexto> tbTipoContextoList;
    @JoinColumn(name = "tipo_con_seq_pai", referencedColumnName = "tipo_con_seq")
    @ManyToOne
    private TbTipoContexto tipoConSeqPai;
    @OneToMany(mappedBy = "tipoConSeq")
    private List<TbVariavel> tbVariavelList;
    @OneToMany(mappedBy = "tipoConSeq")
    private List<TbVariavelDw> tbVariavelDwList;

    public TbTipoContexto() {
    }

    public TbTipoContexto(Integer tipoConSeq) {
        this.tipoConSeq = tipoConSeq;
    }

    public Integer getTipoConSeq() {
        return tipoConSeq;
    }

    public void setTipoConSeq(Integer tipoConSeq) {
        this.tipoConSeq = tipoConSeq;
    }

    public String getTipoConNome() {
        return tipoConNome;
    }

    public void setTipoConNome(String tipoConNome) {
        this.tipoConNome = tipoConNome;
    }

    public String getTipoConCodigo() {
        return tipoConCodigo;
    }

    public void setTipoConCodigo(String tipoConCodigo) {
        this.tipoConCodigo = tipoConCodigo;
    }

    @XmlTransient
    public List<TbTipoContexto> getTbTipoContextoList() {
        return tbTipoContextoList;
    }

    public void setTbTipoContextoList(List<TbTipoContexto> tbTipoContextoList) {
        this.tbTipoContextoList = tbTipoContextoList;
    }

    public TbTipoContexto getTipoConSeqPai() {
        return tipoConSeqPai;
    }

    public void setTipoConSeqPai(TbTipoContexto tipoConSeqPai) {
        this.tipoConSeqPai = tipoConSeqPai;
    }

    @XmlTransient
    public List<TbVariavel> getTbVariavelList() {
        return tbVariavelList;
    }

    public void setTbVariavelList(List<TbVariavel> tbVariavelList) {
        this.tbVariavelList = tbVariavelList;
    }

    @XmlTransient
    public List<TbVariavelDw> getTbVariavelDwList() {
        return tbVariavelDwList;
    }

    public void setTbVariavelDwList(List<TbVariavelDw> tbVariavelDwList) {
        this.tbVariavelDwList = tbVariavelDwList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoConSeq != null ? tipoConSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTipoContexto)) {
            return false;
        }
        TbTipoContexto other = (TbTipoContexto) object;
        if ((this.tipoConSeq == null && other.tipoConSeq != null) || (this.tipoConSeq != null && !this.tipoConSeq.equals(other.tipoConSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTipoContexto[ tipoConSeq=" + tipoConSeq + " ]";
    }
    
}
