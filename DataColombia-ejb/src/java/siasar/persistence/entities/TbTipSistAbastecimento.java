/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_tip_sist_abastecimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTipSistAbastecimento.findAll", query = "SELECT t FROM TbTipSistAbastecimento t")
    , @NamedQuery(name = "TbTipSistAbastecimento.findBySisteSeq", query = "SELECT t FROM TbTipSistAbastecimento t WHERE t.tbTipSistAbastecimentoPK.sisteSeq = :sisteSeq")
    , @NamedQuery(name = "TbTipSistAbastecimento.findByTaxtSeq", query = "SELECT t FROM TbTipSistAbastecimento t WHERE t.tbTipSistAbastecimentoPK.taxtSeq = :taxtSeq")
    , @NamedQuery(name = "TbTipSistAbastecimento.findBySysA027001", query = "SELECT t FROM TbTipSistAbastecimento t WHERE t.sysA027001 = :sysA027001")
    , @NamedQuery(name = "TbTipSistAbastecimento.findByDateLastUpdate", query = "SELECT t FROM TbTipSistAbastecimento t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbTipSistAbastecimento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TbTipSistAbastecimentoPK tbTipSistAbastecimentoPK;
    @Column(name = "sys_a_027_001")
    private Integer sysA027001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TbSistema tbSistema;
    @JoinColumn(name = "taxt_seq", referencedColumnName = "taxt_seq", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TbTaxoTermo tbTaxoTermo;

    public TbTipSistAbastecimento() {
    }

    public TbTipSistAbastecimento(TbTipSistAbastecimentoPK tbTipSistAbastecimentoPK) {
        this.tbTipSistAbastecimentoPK = tbTipSistAbastecimentoPK;
    }

    public TbTipSistAbastecimento(long sisteSeq, long taxtSeq) {
        this.tbTipSistAbastecimentoPK = new TbTipSistAbastecimentoPK(sisteSeq, taxtSeq);
    }

    public TbTipSistAbastecimentoPK getTbTipSistAbastecimentoPK() {
        return tbTipSistAbastecimentoPK;
    }

    public void setTbTipSistAbastecimentoPK(TbTipSistAbastecimentoPK tbTipSistAbastecimentoPK) {
        this.tbTipSistAbastecimentoPK = tbTipSistAbastecimentoPK;
    }

    public Integer getSysA027001() {
        return sysA027001;
    }

    public void setSysA027001(Integer sysA027001) {
        this.sysA027001 = sysA027001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getTbSistema() {
        return tbSistema;
    }

    public void setTbSistema(TbSistema tbSistema) {
        this.tbSistema = tbSistema;
    }

    public TbTaxoTermo getTbTaxoTermo() {
        return tbTaxoTermo;
    }

    public void setTbTaxoTermo(TbTaxoTermo tbTaxoTermo) {
        this.tbTaxoTermo = tbTaxoTermo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbTipSistAbastecimentoPK != null ? tbTipSistAbastecimentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTipSistAbastecimento)) {
            return false;
        }
        TbTipSistAbastecimento other = (TbTipSistAbastecimento) object;
        if ((this.tbTipSistAbastecimentoPK == null && other.tbTipSistAbastecimentoPK != null) || (this.tbTipSistAbastecimentoPK != null && !this.tbTipSistAbastecimentoPK.equals(other.tbTipSistAbastecimentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTipSistAbastecimento[ tbTipSistAbastecimentoPK=" + tbTipSistAbastecimentoPK + " ]";
    }
    
}
