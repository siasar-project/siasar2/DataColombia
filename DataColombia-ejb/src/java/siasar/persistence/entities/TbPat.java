/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_pat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPat.findAll", query = "SELECT t FROM TbPat t")
    , @NamedQuery(name = "TbPat.findByPatSeq", query = "SELECT t FROM TbPat t WHERE t.patSeq = :patSeq")
    , @NamedQuery(name = "TbPat.findByValidado", query = "SELECT t FROM TbPat t WHERE t.validado = :validado")
    , @NamedQuery(name = "TbPat.findByDataValidacao", query = "SELECT t FROM TbPat t WHERE t.dataValidacao = :dataValidacao")
    , @NamedQuery(name = "TbPat.findByPatA001001", query = "SELECT t FROM TbPat t WHERE t.patA001001 = :patA001001")
    , @NamedQuery(name = "TbPat.findByPatA002001", query = "SELECT t FROM TbPat t WHERE t.patA002001 = :patA002001")
    , @NamedQuery(name = "TbPat.findByPatA003001", query = "SELECT t FROM TbPat t WHERE t.patA003001 = :patA003001")
    , @NamedQuery(name = "TbPat.findByPatA004001", query = "SELECT t FROM TbPat t WHERE t.patA004001 = :patA004001")
    , @NamedQuery(name = "TbPat.findByPatA005001", query = "SELECT t FROM TbPat t WHERE t.patA005001 = :patA005001")
    , @NamedQuery(name = "TbPat.findByPatA006001", query = "SELECT t FROM TbPat t WHERE t.patA006001 = :patA006001")
    , @NamedQuery(name = "TbPat.findByPatA007001", query = "SELECT t FROM TbPat t WHERE t.patA007001 = :patA007001")
    , @NamedQuery(name = "TbPat.findByPatA008001", query = "SELECT t FROM TbPat t WHERE t.patA008001 = :patA008001")
    , @NamedQuery(name = "TbPat.findByPatA009001", query = "SELECT t FROM TbPat t WHERE t.patA009001 = :patA009001")
    , @NamedQuery(name = "TbPat.findByPatB001001", query = "SELECT t FROM TbPat t WHERE t.patB001001 = :patB001001")
    , @NamedQuery(name = "TbPat.findByPatB002001", query = "SELECT t FROM TbPat t WHERE t.patB002001 = :patB002001")
    , @NamedQuery(name = "TbPat.findByPatC001001", query = "SELECT t FROM TbPat t WHERE t.patC001001 = :patC001001")
    , @NamedQuery(name = "TbPat.findByPatC002001", query = "SELECT t FROM TbPat t WHERE t.patC002001 = :patC002001")
    , @NamedQuery(name = "TbPat.findByPatC003001", query = "SELECT t FROM TbPat t WHERE t.patC003001 = :patC003001")
    , @NamedQuery(name = "TbPat.findByPatC004001", query = "SELECT t FROM TbPat t WHERE t.patC004001 = :patC004001")
    , @NamedQuery(name = "TbPat.findByPatC005001", query = "SELECT t FROM TbPat t WHERE t.patC005001 = :patC005001")
    , @NamedQuery(name = "TbPat.findByPatC006001", query = "SELECT t FROM TbPat t WHERE t.patC006001 = :patC006001")
    , @NamedQuery(name = "TbPat.findByPatC007001", query = "SELECT t FROM TbPat t WHERE t.patC007001 = :patC007001")
    , @NamedQuery(name = "TbPat.findByPatC008001", query = "SELECT t FROM TbPat t WHERE t.patC008001 = :patC008001")
    , @NamedQuery(name = "TbPat.findByPatC009001", query = "SELECT t FROM TbPat t WHERE t.patC009001 = :patC009001")
    , @NamedQuery(name = "TbPat.findByPatC010001", query = "SELECT t FROM TbPat t WHERE t.patC010001 = :patC010001")
    , @NamedQuery(name = "TbPat.findByPatC011001", query = "SELECT t FROM TbPat t WHERE t.patC011001 = :patC011001")
    , @NamedQuery(name = "TbPat.findByPatC012001", query = "SELECT t FROM TbPat t WHERE t.patC012001 = :patC012001")
    , @NamedQuery(name = "TbPat.findByPatC013001", query = "SELECT t FROM TbPat t WHERE t.patC013001 = :patC013001")
    , @NamedQuery(name = "TbPat.findByPatC014001", query = "SELECT t FROM TbPat t WHERE t.patC014001 = :patC014001")
    , @NamedQuery(name = "TbPat.findByPatC015001", query = "SELECT t FROM TbPat t WHERE t.patC015001 = :patC015001")
    , @NamedQuery(name = "TbPat.findByPatC016001", query = "SELECT t FROM TbPat t WHERE t.patC016001 = :patC016001")
    , @NamedQuery(name = "TbPat.findByPatC017001", query = "SELECT t FROM TbPat t WHERE t.patC017001 = :patC017001")
    , @NamedQuery(name = "TbPat.findByPatC018001", query = "SELECT t FROM TbPat t WHERE t.patC018001 = :patC018001")
    , @NamedQuery(name = "TbPat.findByPatD001001", query = "SELECT t FROM TbPat t WHERE t.patD001001 = :patD001001")
    , @NamedQuery(name = "TbPat.findByPatD002001", query = "SELECT t FROM TbPat t WHERE t.patD002001 = :patD002001")
    , @NamedQuery(name = "TbPat.findByPatD003001", query = "SELECT t FROM TbPat t WHERE t.patD003001 = :patD003001")
    , @NamedQuery(name = "TbPat.findByPatD004001", query = "SELECT t FROM TbPat t WHERE t.patD004001 = :patD004001")
    , @NamedQuery(name = "TbPat.findByPatD005001", query = "SELECT t FROM TbPat t WHERE t.patD005001 = :patD005001")
    , @NamedQuery(name = "TbPat.findByPatD006001", query = "SELECT t FROM TbPat t WHERE t.patD006001 = :patD006001")
    , @NamedQuery(name = "TbPat.findByPatD007001", query = "SELECT t FROM TbPat t WHERE t.patD007001 = :patD007001")
    , @NamedQuery(name = "TbPat.findByPatD008001", query = "SELECT t FROM TbPat t WHERE t.patD008001 = :patD008001")
    , @NamedQuery(name = "TbPat.findByPatD009001", query = "SELECT t FROM TbPat t WHERE t.patD009001 = :patD009001")
    , @NamedQuery(name = "TbPat.findByPatD010001", query = "SELECT t FROM TbPat t WHERE t.patD010001 = :patD010001")
    , @NamedQuery(name = "TbPat.findByPatD011001", query = "SELECT t FROM TbPat t WHERE t.patD011001 = :patD011001")
    , @NamedQuery(name = "TbPat.findByPatD012001", query = "SELECT t FROM TbPat t WHERE t.patD012001 = :patD012001")
    , @NamedQuery(name = "TbPat.findByPatD013001", query = "SELECT t FROM TbPat t WHERE t.patD013001 = :patD013001")
    , @NamedQuery(name = "TbPat.findByPatD014001", query = "SELECT t FROM TbPat t WHERE t.patD014001 = :patD014001")
    , @NamedQuery(name = "TbPat.findByPatE001001", query = "SELECT t FROM TbPat t WHERE t.patE001001 = :patE001001")
    , @NamedQuery(name = "TbPat.findByPaisSigla", query = "SELECT t FROM TbPat t WHERE t.paisSigla = :paisSigla")
    , @NamedQuery(name = "TbPat.findByDateLastUpdate", query = "SELECT t FROM TbPat t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbPat.findByPatA010001", query = "SELECT t FROM TbPat t WHERE t.patA010001 = :patA010001")
    , @NamedQuery(name = "TbPat.findByPatA011001", query = "SELECT t FROM TbPat t WHERE t.patA011001 = :patA011001")
    , @NamedQuery(name = "TbPat.findByClassificacao", query = "SELECT t FROM TbPat t WHERE t.classificacao = :classificacao")})
public class TbPat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pat_seq")
    private Integer patSeq;
    @Column(name = "validado")
    private Boolean validado;
    @Column(name = "data_validacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataValidacao;
    @Size(max = 50)
    @Column(name = "pat_a_001_001")
    private String patA001001;
    @Column(name = "pat_a_002_001")
    @Temporal(TemporalType.DATE)
    private Date patA002001;
    @Size(max = 80)
    @Column(name = "pat_a_003_001")
    private String patA003001;
    @Size(max = 1000)
    @Column(name = "pat_a_004_001")
    private String patA004001;
    @Size(max = 300)
    @Column(name = "pat_a_005_001")
    private String patA005001;
    @Column(name = "pat_a_006_001")
    private Integer patA006001;
    @Column(name = "pat_a_007_001")
    private Integer patA007001;
    @Column(name = "pat_a_008_001")
    private Integer patA008001;
    @Size(max = 2)
    @Column(name = "pat_a_009_001")
    private String patA009001;
    @Column(name = "pat_b_001_001")
    private Integer patB001001;
    @Column(name = "pat_b_002_001")
    private Integer patB002001;
    @Column(name = "pat_c_001_001")
    private Integer patC001001;
    @Column(name = "pat_c_002_001")
    private Boolean patC002001;
    @Column(name = "pat_c_003_001")
    private Integer patC003001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "pat_c_004_001")
    private BigDecimal patC004001;
    @Column(name = "pat_c_005_001")
    private Integer patC005001;
    @Column(name = "pat_c_006_001")
    private Integer patC006001;
    @Column(name = "pat_c_007_001")
    private Integer patC007001;
    @Column(name = "pat_c_008_001")
    private Integer patC008001;
    @Column(name = "pat_c_009_001")
    private Integer patC009001;
    @Column(name = "pat_c_010_001")
    private Integer patC010001;
    @Column(name = "pat_c_011_001")
    private Boolean patC011001;
    @Column(name = "pat_c_012_001")
    private Integer patC012001;
    @Column(name = "pat_c_013_001")
    private Boolean patC013001;
    @Column(name = "pat_c_014_001")
    private Integer patC014001;
    @Column(name = "pat_c_015_001")
    private Boolean patC015001;
    @Column(name = "pat_c_016_001")
    private Integer patC016001;
    @Column(name = "pat_c_017_001")
    private Boolean patC017001;
    @Column(name = "pat_c_018_001")
    private Integer patC018001;
    @Column(name = "pat_d_001_001")
    private Integer patD001001;
    @Column(name = "pat_d_002_001")
    private Integer patD002001;
    @Column(name = "pat_d_003_001")
    private Integer patD003001;
    @Column(name = "pat_d_004_001")
    private Integer patD004001;
    @Column(name = "pat_d_005_001")
    private Integer patD005001;
    @Column(name = "pat_d_006_001")
    private Integer patD006001;
    @Column(name = "pat_d_007_001")
    private Integer patD007001;
    @Column(name = "pat_d_008_001")
    private Integer patD008001;
    @Column(name = "pat_d_009_001")
    private Integer patD009001;
    @Column(name = "pat_d_010_001")
    private Integer patD010001;
    @Column(name = "pat_d_011_001")
    private Integer patD011001;
    @Column(name = "pat_d_012_001")
    private Integer patD012001;
    @Size(max = 3000)
    @Column(name = "pat_d_013_001")
    private String patD013001;
    @Column(name = "pat_d_014_001")
    private Integer patD014001;
    @Size(max = 4000)
    @Column(name = "pat_e_001_001")
    private String patE001001;
    @Size(max = 2)
    @Column(name = "pais_sigla")
    private String paisSigla;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Column(name = "pat_a_010_001")
    private Double patA010001;
    @Column(name = "pat_a_011_001")
    private Double patA011001;
    @Size(max = 2147483647)
    @Column(name = "classificacao")
    private String classificacao;

    public TbPat() {
    }

    public TbPat(Integer patSeq) {
        this.patSeq = patSeq;
    }

    public Integer getPatSeq() {
        return patSeq;
    }

    public void setPatSeq(Integer patSeq) {
        this.patSeq = patSeq;
    }

    public Boolean getValidado() {
        return validado;
    }

    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public String getPatA001001() {
        return patA001001;
    }

    public void setPatA001001(String patA001001) {
        this.patA001001 = patA001001;
    }

    public Date getPatA002001() {
        return patA002001;
    }

    public void setPatA002001(Date patA002001) {
        this.patA002001 = patA002001;
    }

    public String getPatA003001() {
        return patA003001;
    }

    public void setPatA003001(String patA003001) {
        this.patA003001 = patA003001;
    }

    public String getPatA004001() {
        return patA004001;
    }

    public void setPatA004001(String patA004001) {
        this.patA004001 = patA004001;
    }

    public String getPatA005001() {
        return patA005001;
    }

    public void setPatA005001(String patA005001) {
        this.patA005001 = patA005001;
    }

    public Integer getPatA006001() {
        return patA006001;
    }

    public void setPatA006001(Integer patA006001) {
        this.patA006001 = patA006001;
    }

    public Integer getPatA007001() {
        return patA007001;
    }

    public void setPatA007001(Integer patA007001) {
        this.patA007001 = patA007001;
    }

    public Integer getPatA008001() {
        return patA008001;
    }

    public void setPatA008001(Integer patA008001) {
        this.patA008001 = patA008001;
    }

    public String getPatA009001() {
        return patA009001;
    }

    public void setPatA009001(String patA009001) {
        this.patA009001 = patA009001;
    }

    public Integer getPatB001001() {
        return patB001001;
    }

    public void setPatB001001(Integer patB001001) {
        this.patB001001 = patB001001;
    }

    public Integer getPatB002001() {
        return patB002001;
    }

    public void setPatB002001(Integer patB002001) {
        this.patB002001 = patB002001;
    }

    public Integer getPatC001001() {
        return patC001001;
    }

    public void setPatC001001(Integer patC001001) {
        this.patC001001 = patC001001;
    }

    public Boolean getPatC002001() {
        return patC002001;
    }

    public void setPatC002001(Boolean patC002001) {
        this.patC002001 = patC002001;
    }

    public Integer getPatC003001() {
        return patC003001;
    }

    public void setPatC003001(Integer patC003001) {
        this.patC003001 = patC003001;
    }

    public BigDecimal getPatC004001() {
        return patC004001;
    }

    public void setPatC004001(BigDecimal patC004001) {
        this.patC004001 = patC004001;
    }

    public Integer getPatC005001() {
        return patC005001;
    }

    public void setPatC005001(Integer patC005001) {
        this.patC005001 = patC005001;
    }

    public Integer getPatC006001() {
        return patC006001;
    }

    public void setPatC006001(Integer patC006001) {
        this.patC006001 = patC006001;
    }

    public Integer getPatC007001() {
        return patC007001;
    }

    public void setPatC007001(Integer patC007001) {
        this.patC007001 = patC007001;
    }

    public Integer getPatC008001() {
        return patC008001;
    }

    public void setPatC008001(Integer patC008001) {
        this.patC008001 = patC008001;
    }

    public Integer getPatC009001() {
        return patC009001;
    }

    public void setPatC009001(Integer patC009001) {
        this.patC009001 = patC009001;
    }

    public Integer getPatC010001() {
        return patC010001;
    }

    public void setPatC010001(Integer patC010001) {
        this.patC010001 = patC010001;
    }

    public Boolean getPatC011001() {
        return patC011001;
    }

    public void setPatC011001(Boolean patC011001) {
        this.patC011001 = patC011001;
    }

    public Integer getPatC012001() {
        return patC012001;
    }

    public void setPatC012001(Integer patC012001) {
        this.patC012001 = patC012001;
    }

    public Boolean getPatC013001() {
        return patC013001;
    }

    public void setPatC013001(Boolean patC013001) {
        this.patC013001 = patC013001;
    }

    public Integer getPatC014001() {
        return patC014001;
    }

    public void setPatC014001(Integer patC014001) {
        this.patC014001 = patC014001;
    }

    public Boolean getPatC015001() {
        return patC015001;
    }

    public void setPatC015001(Boolean patC015001) {
        this.patC015001 = patC015001;
    }

    public Integer getPatC016001() {
        return patC016001;
    }

    public void setPatC016001(Integer patC016001) {
        this.patC016001 = patC016001;
    }

    public Boolean getPatC017001() {
        return patC017001;
    }

    public void setPatC017001(Boolean patC017001) {
        this.patC017001 = patC017001;
    }

    public Integer getPatC018001() {
        return patC018001;
    }

    public void setPatC018001(Integer patC018001) {
        this.patC018001 = patC018001;
    }

    public Integer getPatD001001() {
        return patD001001;
    }

    public void setPatD001001(Integer patD001001) {
        this.patD001001 = patD001001;
    }

    public Integer getPatD002001() {
        return patD002001;
    }

    public void setPatD002001(Integer patD002001) {
        this.patD002001 = patD002001;
    }

    public Integer getPatD003001() {
        return patD003001;
    }

    public void setPatD003001(Integer patD003001) {
        this.patD003001 = patD003001;
    }

    public Integer getPatD004001() {
        return patD004001;
    }

    public void setPatD004001(Integer patD004001) {
        this.patD004001 = patD004001;
    }

    public Integer getPatD005001() {
        return patD005001;
    }

    public void setPatD005001(Integer patD005001) {
        this.patD005001 = patD005001;
    }

    public Integer getPatD006001() {
        return patD006001;
    }

    public void setPatD006001(Integer patD006001) {
        this.patD006001 = patD006001;
    }

    public Integer getPatD007001() {
        return patD007001;
    }

    public void setPatD007001(Integer patD007001) {
        this.patD007001 = patD007001;
    }

    public Integer getPatD008001() {
        return patD008001;
    }

    public void setPatD008001(Integer patD008001) {
        this.patD008001 = patD008001;
    }

    public Integer getPatD009001() {
        return patD009001;
    }

    public void setPatD009001(Integer patD009001) {
        this.patD009001 = patD009001;
    }

    public Integer getPatD010001() {
        return patD010001;
    }

    public void setPatD010001(Integer patD010001) {
        this.patD010001 = patD010001;
    }

    public Integer getPatD011001() {
        return patD011001;
    }

    public void setPatD011001(Integer patD011001) {
        this.patD011001 = patD011001;
    }

    public Integer getPatD012001() {
        return patD012001;
    }

    public void setPatD012001(Integer patD012001) {
        this.patD012001 = patD012001;
    }

    public String getPatD013001() {
        return patD013001;
    }

    public void setPatD013001(String patD013001) {
        this.patD013001 = patD013001;
    }

    public Integer getPatD014001() {
        return patD014001;
    }

    public void setPatD014001(Integer patD014001) {
        this.patD014001 = patD014001;
    }

    public String getPatE001001() {
        return patE001001;
    }

    public void setPatE001001(String patE001001) {
        this.patE001001 = patE001001;
    }

    public String getPaisSigla() {
        return paisSigla;
    }

    public void setPaisSigla(String paisSigla) {
        this.paisSigla = paisSigla;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public Double getPatA010001() {
        return patA010001;
    }

    public void setPatA010001(Double patA010001) {
        this.patA010001 = patA010001;
    }

    public Double getPatA011001() {
        return patA011001;
    }

    public void setPatA011001(Double patA011001) {
        this.patA011001 = patA011001;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patSeq != null ? patSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPat)) {
            return false;
        }
        TbPat other = (TbPat) object;
        if ((this.patSeq == null && other.patSeq != null) || (this.patSeq != null && !this.patSeq.equals(other.patSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbPat[ patSeq=" + patSeq + " ]";
    }
    
}
