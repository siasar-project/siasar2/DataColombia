/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "admin_interface_theme")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminInterfaceTheme.findAll", query = "SELECT a FROM AdminInterfaceTheme a")
    , @NamedQuery(name = "AdminInterfaceTheme.findById", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.id = :id")
    , @NamedQuery(name = "AdminInterfaceTheme.findByName", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.name = :name")
    , @NamedQuery(name = "AdminInterfaceTheme.findByActive", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.active = :active")
    , @NamedQuery(name = "AdminInterfaceTheme.findByTitle", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.title = :title")
    , @NamedQuery(name = "AdminInterfaceTheme.findByTitleVisible", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.titleVisible = :titleVisible")
    , @NamedQuery(name = "AdminInterfaceTheme.findByLogo", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.logo = :logo")
    , @NamedQuery(name = "AdminInterfaceTheme.findByLogoVisible", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.logoVisible = :logoVisible")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssHeaderBackgroundColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssHeaderBackgroundColor = :cssHeaderBackgroundColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByTitleColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.titleColor = :titleColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssHeaderTextColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssHeaderTextColor = :cssHeaderTextColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssHeaderLinkColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssHeaderLinkColor = :cssHeaderLinkColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssHeaderLinkHoverColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssHeaderLinkHoverColor = :cssHeaderLinkHoverColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssModuleBackgroundColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssModuleBackgroundColor = :cssModuleBackgroundColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssModuleTextColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssModuleTextColor = :cssModuleTextColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssModuleLinkColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssModuleLinkColor = :cssModuleLinkColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssModuleLinkHoverColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssModuleLinkHoverColor = :cssModuleLinkHoverColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssModuleRoundedCorners", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssModuleRoundedCorners = :cssModuleRoundedCorners")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssGenericLinkColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssGenericLinkColor = :cssGenericLinkColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssGenericLinkHoverColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssGenericLinkHoverColor = :cssGenericLinkHoverColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssSaveButtonBackgroundColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssSaveButtonBackgroundColor = :cssSaveButtonBackgroundColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssSaveButtonBackgroundHoverColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssSaveButtonBackgroundHoverColor = :cssSaveButtonBackgroundHoverColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssSaveButtonTextColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssSaveButtonTextColor = :cssSaveButtonTextColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssDeleteButtonBackgroundColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssDeleteButtonBackgroundColor = :cssDeleteButtonBackgroundColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssDeleteButtonBackgroundHoverColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssDeleteButtonBackgroundHoverColor = :cssDeleteButtonBackgroundHoverColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCssDeleteButtonTextColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.cssDeleteButtonTextColor = :cssDeleteButtonTextColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByCss", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.css = :css")
    , @NamedQuery(name = "AdminInterfaceTheme.findByListFilterDropdown", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.listFilterDropdown = :listFilterDropdown")
    , @NamedQuery(name = "AdminInterfaceTheme.findByRelatedModalActive", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.relatedModalActive = :relatedModalActive")
    , @NamedQuery(name = "AdminInterfaceTheme.findByRelatedModalBackgroundColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.relatedModalBackgroundColor = :relatedModalBackgroundColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByRelatedModalBackgroundOpacity", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.relatedModalBackgroundOpacity = :relatedModalBackgroundOpacity")
    , @NamedQuery(name = "AdminInterfaceTheme.findByRelatedModalRoundedCorners", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.relatedModalRoundedCorners = :relatedModalRoundedCorners")
    , @NamedQuery(name = "AdminInterfaceTheme.findByLogoColor", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.logoColor = :logoColor")
    , @NamedQuery(name = "AdminInterfaceTheme.findByRecentActionsVisible", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.recentActionsVisible = :recentActionsVisible")
    , @NamedQuery(name = "AdminInterfaceTheme.findByFavicon", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.favicon = :favicon")
    , @NamedQuery(name = "AdminInterfaceTheme.findByEnv", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.env = :env")
    , @NamedQuery(name = "AdminInterfaceTheme.findByEnvVisible", query = "SELECT a FROM AdminInterfaceTheme a WHERE a.envVisible = :envVisible")})
public class AdminInterfaceTheme implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Column(name = "title_visible")
    private boolean titleVisible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "logo")
    private String logo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "logo_visible")
    private boolean logoVisible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_header_background_color")
    private String cssHeaderBackgroundColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "title_color")
    private String titleColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_header_text_color")
    private String cssHeaderTextColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_header_link_color")
    private String cssHeaderLinkColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_header_link_hover_color")
    private String cssHeaderLinkHoverColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_module_background_color")
    private String cssModuleBackgroundColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_module_text_color")
    private String cssModuleTextColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_module_link_color")
    private String cssModuleLinkColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_module_link_hover_color")
    private String cssModuleLinkHoverColor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "css_module_rounded_corners")
    private boolean cssModuleRoundedCorners;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_generic_link_color")
    private String cssGenericLinkColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_generic_link_hover_color")
    private String cssGenericLinkHoverColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_save_button_background_color")
    private String cssSaveButtonBackgroundColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_save_button_background_hover_color")
    private String cssSaveButtonBackgroundHoverColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_save_button_text_color")
    private String cssSaveButtonTextColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_delete_button_background_color")
    private String cssDeleteButtonBackgroundColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_delete_button_background_hover_color")
    private String cssDeleteButtonBackgroundHoverColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "css_delete_button_text_color")
    private String cssDeleteButtonTextColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "css")
    private String css;
    @Basic(optional = false)
    @NotNull
    @Column(name = "list_filter_dropdown")
    private boolean listFilterDropdown;
    @Basic(optional = false)
    @NotNull
    @Column(name = "related_modal_active")
    private boolean relatedModalActive;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "related_modal_background_color")
    private String relatedModalBackgroundColor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "related_modal_background_opacity")
    private String relatedModalBackgroundOpacity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "related_modal_rounded_corners")
    private boolean relatedModalRoundedCorners;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "logo_color")
    private String logoColor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recent_actions_visible")
    private boolean recentActionsVisible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "favicon")
    private String favicon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "env")
    private String env;
    @Basic(optional = false)
    @NotNull
    @Column(name = "env_visible")
    private boolean envVisible;

    public AdminInterfaceTheme() {
    }

    public AdminInterfaceTheme(Integer id) {
        this.id = id;
    }

    public AdminInterfaceTheme(Integer id, String name, boolean active, String title, boolean titleVisible, String logo, boolean logoVisible, String cssHeaderBackgroundColor, String titleColor, String cssHeaderTextColor, String cssHeaderLinkColor, String cssHeaderLinkHoverColor, String cssModuleBackgroundColor, String cssModuleTextColor, String cssModuleLinkColor, String cssModuleLinkHoverColor, boolean cssModuleRoundedCorners, String cssGenericLinkColor, String cssGenericLinkHoverColor, String cssSaveButtonBackgroundColor, String cssSaveButtonBackgroundHoverColor, String cssSaveButtonTextColor, String cssDeleteButtonBackgroundColor, String cssDeleteButtonBackgroundHoverColor, String cssDeleteButtonTextColor, String css, boolean listFilterDropdown, boolean relatedModalActive, String relatedModalBackgroundColor, String relatedModalBackgroundOpacity, boolean relatedModalRoundedCorners, String logoColor, boolean recentActionsVisible, String favicon, String env, boolean envVisible) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.title = title;
        this.titleVisible = titleVisible;
        this.logo = logo;
        this.logoVisible = logoVisible;
        this.cssHeaderBackgroundColor = cssHeaderBackgroundColor;
        this.titleColor = titleColor;
        this.cssHeaderTextColor = cssHeaderTextColor;
        this.cssHeaderLinkColor = cssHeaderLinkColor;
        this.cssHeaderLinkHoverColor = cssHeaderLinkHoverColor;
        this.cssModuleBackgroundColor = cssModuleBackgroundColor;
        this.cssModuleTextColor = cssModuleTextColor;
        this.cssModuleLinkColor = cssModuleLinkColor;
        this.cssModuleLinkHoverColor = cssModuleLinkHoverColor;
        this.cssModuleRoundedCorners = cssModuleRoundedCorners;
        this.cssGenericLinkColor = cssGenericLinkColor;
        this.cssGenericLinkHoverColor = cssGenericLinkHoverColor;
        this.cssSaveButtonBackgroundColor = cssSaveButtonBackgroundColor;
        this.cssSaveButtonBackgroundHoverColor = cssSaveButtonBackgroundHoverColor;
        this.cssSaveButtonTextColor = cssSaveButtonTextColor;
        this.cssDeleteButtonBackgroundColor = cssDeleteButtonBackgroundColor;
        this.cssDeleteButtonBackgroundHoverColor = cssDeleteButtonBackgroundHoverColor;
        this.cssDeleteButtonTextColor = cssDeleteButtonTextColor;
        this.css = css;
        this.listFilterDropdown = listFilterDropdown;
        this.relatedModalActive = relatedModalActive;
        this.relatedModalBackgroundColor = relatedModalBackgroundColor;
        this.relatedModalBackgroundOpacity = relatedModalBackgroundOpacity;
        this.relatedModalRoundedCorners = relatedModalRoundedCorners;
        this.logoColor = logoColor;
        this.recentActionsVisible = recentActionsVisible;
        this.favicon = favicon;
        this.env = env;
        this.envVisible = envVisible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getTitleVisible() {
        return titleVisible;
    }

    public void setTitleVisible(boolean titleVisible) {
        this.titleVisible = titleVisible;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean getLogoVisible() {
        return logoVisible;
    }

    public void setLogoVisible(boolean logoVisible) {
        this.logoVisible = logoVisible;
    }

    public String getCssHeaderBackgroundColor() {
        return cssHeaderBackgroundColor;
    }

    public void setCssHeaderBackgroundColor(String cssHeaderBackgroundColor) {
        this.cssHeaderBackgroundColor = cssHeaderBackgroundColor;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public String getCssHeaderTextColor() {
        return cssHeaderTextColor;
    }

    public void setCssHeaderTextColor(String cssHeaderTextColor) {
        this.cssHeaderTextColor = cssHeaderTextColor;
    }

    public String getCssHeaderLinkColor() {
        return cssHeaderLinkColor;
    }

    public void setCssHeaderLinkColor(String cssHeaderLinkColor) {
        this.cssHeaderLinkColor = cssHeaderLinkColor;
    }

    public String getCssHeaderLinkHoverColor() {
        return cssHeaderLinkHoverColor;
    }

    public void setCssHeaderLinkHoverColor(String cssHeaderLinkHoverColor) {
        this.cssHeaderLinkHoverColor = cssHeaderLinkHoverColor;
    }

    public String getCssModuleBackgroundColor() {
        return cssModuleBackgroundColor;
    }

    public void setCssModuleBackgroundColor(String cssModuleBackgroundColor) {
        this.cssModuleBackgroundColor = cssModuleBackgroundColor;
    }

    public String getCssModuleTextColor() {
        return cssModuleTextColor;
    }

    public void setCssModuleTextColor(String cssModuleTextColor) {
        this.cssModuleTextColor = cssModuleTextColor;
    }

    public String getCssModuleLinkColor() {
        return cssModuleLinkColor;
    }

    public void setCssModuleLinkColor(String cssModuleLinkColor) {
        this.cssModuleLinkColor = cssModuleLinkColor;
    }

    public String getCssModuleLinkHoverColor() {
        return cssModuleLinkHoverColor;
    }

    public void setCssModuleLinkHoverColor(String cssModuleLinkHoverColor) {
        this.cssModuleLinkHoverColor = cssModuleLinkHoverColor;
    }

    public boolean getCssModuleRoundedCorners() {
        return cssModuleRoundedCorners;
    }

    public void setCssModuleRoundedCorners(boolean cssModuleRoundedCorners) {
        this.cssModuleRoundedCorners = cssModuleRoundedCorners;
    }

    public String getCssGenericLinkColor() {
        return cssGenericLinkColor;
    }

    public void setCssGenericLinkColor(String cssGenericLinkColor) {
        this.cssGenericLinkColor = cssGenericLinkColor;
    }

    public String getCssGenericLinkHoverColor() {
        return cssGenericLinkHoverColor;
    }

    public void setCssGenericLinkHoverColor(String cssGenericLinkHoverColor) {
        this.cssGenericLinkHoverColor = cssGenericLinkHoverColor;
    }

    public String getCssSaveButtonBackgroundColor() {
        return cssSaveButtonBackgroundColor;
    }

    public void setCssSaveButtonBackgroundColor(String cssSaveButtonBackgroundColor) {
        this.cssSaveButtonBackgroundColor = cssSaveButtonBackgroundColor;
    }

    public String getCssSaveButtonBackgroundHoverColor() {
        return cssSaveButtonBackgroundHoverColor;
    }

    public void setCssSaveButtonBackgroundHoverColor(String cssSaveButtonBackgroundHoverColor) {
        this.cssSaveButtonBackgroundHoverColor = cssSaveButtonBackgroundHoverColor;
    }

    public String getCssSaveButtonTextColor() {
        return cssSaveButtonTextColor;
    }

    public void setCssSaveButtonTextColor(String cssSaveButtonTextColor) {
        this.cssSaveButtonTextColor = cssSaveButtonTextColor;
    }

    public String getCssDeleteButtonBackgroundColor() {
        return cssDeleteButtonBackgroundColor;
    }

    public void setCssDeleteButtonBackgroundColor(String cssDeleteButtonBackgroundColor) {
        this.cssDeleteButtonBackgroundColor = cssDeleteButtonBackgroundColor;
    }

    public String getCssDeleteButtonBackgroundHoverColor() {
        return cssDeleteButtonBackgroundHoverColor;
    }

    public void setCssDeleteButtonBackgroundHoverColor(String cssDeleteButtonBackgroundHoverColor) {
        this.cssDeleteButtonBackgroundHoverColor = cssDeleteButtonBackgroundHoverColor;
    }

    public String getCssDeleteButtonTextColor() {
        return cssDeleteButtonTextColor;
    }

    public void setCssDeleteButtonTextColor(String cssDeleteButtonTextColor) {
        this.cssDeleteButtonTextColor = cssDeleteButtonTextColor;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public boolean getListFilterDropdown() {
        return listFilterDropdown;
    }

    public void setListFilterDropdown(boolean listFilterDropdown) {
        this.listFilterDropdown = listFilterDropdown;
    }

    public boolean getRelatedModalActive() {
        return relatedModalActive;
    }

    public void setRelatedModalActive(boolean relatedModalActive) {
        this.relatedModalActive = relatedModalActive;
    }

    public String getRelatedModalBackgroundColor() {
        return relatedModalBackgroundColor;
    }

    public void setRelatedModalBackgroundColor(String relatedModalBackgroundColor) {
        this.relatedModalBackgroundColor = relatedModalBackgroundColor;
    }

    public String getRelatedModalBackgroundOpacity() {
        return relatedModalBackgroundOpacity;
    }

    public void setRelatedModalBackgroundOpacity(String relatedModalBackgroundOpacity) {
        this.relatedModalBackgroundOpacity = relatedModalBackgroundOpacity;
    }

    public boolean getRelatedModalRoundedCorners() {
        return relatedModalRoundedCorners;
    }

    public void setRelatedModalRoundedCorners(boolean relatedModalRoundedCorners) {
        this.relatedModalRoundedCorners = relatedModalRoundedCorners;
    }

    public String getLogoColor() {
        return logoColor;
    }

    public void setLogoColor(String logoColor) {
        this.logoColor = logoColor;
    }

    public boolean getRecentActionsVisible() {
        return recentActionsVisible;
    }

    public void setRecentActionsVisible(boolean recentActionsVisible) {
        this.recentActionsVisible = recentActionsVisible;
    }

    public String getFavicon() {
        return favicon;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public boolean getEnvVisible() {
        return envVisible;
    }

    public void setEnvVisible(boolean envVisible) {
        this.envVisible = envVisible;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminInterfaceTheme)) {
            return false;
        }
        AdminInterfaceTheme other = (AdminInterfaceTheme) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.AdminInterfaceTheme[ id=" + id + " ]";
    }
    
}
