/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_dir_representante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDirRepresentante.findAll", query = "SELECT t FROM TbDirRepresentante t")
    , @NamedQuery(name = "TbDirRepresentante.findByDrepSeq", query = "SELECT t FROM TbDirRepresentante t WHERE t.drepSeq = :drepSeq")
    , @NamedQuery(name = "TbDirRepresentante.findBySepB007001", query = "SELECT t FROM TbDirRepresentante t WHERE t.sepB007001 = :sepB007001")
    , @NamedQuery(name = "TbDirRepresentante.findBySepB009001", query = "SELECT t FROM TbDirRepresentante t WHERE t.sepB009001 = :sepB009001")
    , @NamedQuery(name = "TbDirRepresentante.findByDateLastUpdate", query = "SELECT t FROM TbDirRepresentante t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbDirRepresentante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "drep_seq")
    private Long drepSeq;
    @Size(max = 400)
    @Column(name = "sep_b_007_001")
    private String sepB007001;
    @Size(max = 50)
    @Column(name = "sep_b_009_001")
    private String sepB009001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "prser_seq", referencedColumnName = "prser_seq")
    @ManyToOne
    private TbPrestServico prserSeq;
    @JoinColumn(name = "sep_b_006_001", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo sepB006001;
    @JoinColumn(name = "sep_b_008_001", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo sepB008001;

    public TbDirRepresentante() {
    }

    public TbDirRepresentante(Long drepSeq) {
        this.drepSeq = drepSeq;
    }

    public Long getDrepSeq() {
        return drepSeq;
    }

    public void setDrepSeq(Long drepSeq) {
        this.drepSeq = drepSeq;
    }

    public String getSepB007001() {
        return sepB007001;
    }

    public void setSepB007001(String sepB007001) {
        this.sepB007001 = sepB007001;
    }

    public String getSepB009001() {
        return sepB009001;
    }

    public void setSepB009001(String sepB009001) {
        this.sepB009001 = sepB009001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbPrestServico getPrserSeq() {
        return prserSeq;
    }

    public void setPrserSeq(TbPrestServico prserSeq) {
        this.prserSeq = prserSeq;
    }

    public TbTaxoTermo getSepB006001() {
        return sepB006001;
    }

    public void setSepB006001(TbTaxoTermo sepB006001) {
        this.sepB006001 = sepB006001;
    }

    public TbTaxoTermo getSepB008001() {
        return sepB008001;
    }

    public void setSepB008001(TbTaxoTermo sepB008001) {
        this.sepB008001 = sepB008001;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drepSeq != null ? drepSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDirRepresentante)) {
            return false;
        }
        TbDirRepresentante other = (TbDirRepresentante) object;
        if ((this.drepSeq == null && other.drepSeq != null) || (this.drepSeq != null && !this.drepSeq.equals(other.drepSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbDirRepresentante[ drepSeq=" + drepSeq + " ]";
    }
    
}
