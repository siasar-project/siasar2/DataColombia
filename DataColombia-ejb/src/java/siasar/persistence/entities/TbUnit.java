/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_unit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbUnit.findAll", query = "SELECT t FROM TbUnit t")
    , @NamedQuery(name = "TbUnit.findByUnitSeq", query = "SELECT t FROM TbUnit t WHERE t.unitSeq = :unitSeq")
    , @NamedQuery(name = "TbUnit.findByUnitLabel", query = "SELECT t FROM TbUnit t WHERE t.unitLabel = :unitLabel")
    , @NamedQuery(name = "TbUnit.findByUnitSymbol", query = "SELECT t FROM TbUnit t WHERE t.unitSymbol = :unitSymbol")
    , @NamedQuery(name = "TbUnit.findByUnitFactor", query = "SELECT t FROM TbUnit t WHERE t.unitFactor = :unitFactor")
    , @NamedQuery(name = "TbUnit.findByUnitMeasure", query = "SELECT t FROM TbUnit t WHERE t.unitMeasure = :unitMeasure")
    , @NamedQuery(name = "TbUnit.findByUnitModule", query = "SELECT t FROM TbUnit t WHERE t.unitModule = :unitModule")})
public class TbUnit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "unit_seq")
    private Integer unitSeq;
    @Size(max = 255)
    @Column(name = "unit_label")
    private String unitLabel;
    @Size(max = 40)
    @Column(name = "unit_symbol")
    private String unitSymbol;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unit_factor")
    private BigDecimal unitFactor;
    @Size(max = 50)
    @Column(name = "unit_measure")
    private String unitMeasure;
    @Size(max = 150)
    @Column(name = "unit_module")
    private String unitModule;
    @OneToMany(mappedBy = "unitSeq")
    private List<TbVariavel> tbVariavelList;

    public TbUnit() {
    }

    public TbUnit(Integer unitSeq) {
        this.unitSeq = unitSeq;
    }

    public Integer getUnitSeq() {
        return unitSeq;
    }

    public void setUnitSeq(Integer unitSeq) {
        this.unitSeq = unitSeq;
    }

    public String getUnitLabel() {
        return unitLabel;
    }

    public void setUnitLabel(String unitLabel) {
        this.unitLabel = unitLabel;
    }

    public String getUnitSymbol() {
        return unitSymbol;
    }

    public void setUnitSymbol(String unitSymbol) {
        this.unitSymbol = unitSymbol;
    }

    public BigDecimal getUnitFactor() {
        return unitFactor;
    }

    public void setUnitFactor(BigDecimal unitFactor) {
        this.unitFactor = unitFactor;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getUnitModule() {
        return unitModule;
    }

    public void setUnitModule(String unitModule) {
        this.unitModule = unitModule;
    }

    @XmlTransient
    public List<TbVariavel> getTbVariavelList() {
        return tbVariavelList;
    }

    public void setTbVariavelList(List<TbVariavel> tbVariavelList) {
        this.tbVariavelList = tbVariavelList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unitSeq != null ? unitSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbUnit)) {
            return false;
        }
        TbUnit other = (TbUnit) object;
        if ((this.unitSeq == null && other.unitSeq != null) || (this.unitSeq != null && !this.unitSeq.equals(other.unitSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbUnit[ unitSeq=" + unitSeq + " ]";
    }
    
}
