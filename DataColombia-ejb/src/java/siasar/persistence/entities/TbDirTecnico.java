/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_dir_tecnico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDirTecnico.findAll", query = "SELECT t FROM TbDirTecnico t")
    , @NamedQuery(name = "TbDirTecnico.findByDtecSeq", query = "SELECT t FROM TbDirTecnico t WHERE t.dtecSeq = :dtecSeq")
    , @NamedQuery(name = "TbDirTecnico.findBySepB011001", query = "SELECT t FROM TbDirTecnico t WHERE t.sepB011001 = :sepB011001")
    , @NamedQuery(name = "TbDirTecnico.findBySepB013001", query = "SELECT t FROM TbDirTecnico t WHERE t.sepB013001 = :sepB013001")
    , @NamedQuery(name = "TbDirTecnico.findByDateLastUpdate", query = "SELECT t FROM TbDirTecnico t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbDirTecnico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dtec_seq")
    private Long dtecSeq;
    @Size(max = 400)
    @Column(name = "sep_b_011_001")
    private String sepB011001;
    @Size(max = 50)
    @Column(name = "sep_b_013_001")
    private String sepB013001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "prser_seq", referencedColumnName = "prser_seq")
    @ManyToOne
    private TbPrestServico prserSeq;
    @JoinColumn(name = "sep_b_010_001", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo sepB010001;
    @JoinColumn(name = "sep_b_012_001", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo sepB012001;

    public TbDirTecnico() {
    }

    public TbDirTecnico(Long dtecSeq) {
        this.dtecSeq = dtecSeq;
    }

    public Long getDtecSeq() {
        return dtecSeq;
    }

    public void setDtecSeq(Long dtecSeq) {
        this.dtecSeq = dtecSeq;
    }

    public String getSepB011001() {
        return sepB011001;
    }

    public void setSepB011001(String sepB011001) {
        this.sepB011001 = sepB011001;
    }

    public String getSepB013001() {
        return sepB013001;
    }

    public void setSepB013001(String sepB013001) {
        this.sepB013001 = sepB013001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbPrestServico getPrserSeq() {
        return prserSeq;
    }

    public void setPrserSeq(TbPrestServico prserSeq) {
        this.prserSeq = prserSeq;
    }

    public TbTaxoTermo getSepB010001() {
        return sepB010001;
    }

    public void setSepB010001(TbTaxoTermo sepB010001) {
        this.sepB010001 = sepB010001;
    }

    public TbTaxoTermo getSepB012001() {
        return sepB012001;
    }

    public void setSepB012001(TbTaxoTermo sepB012001) {
        this.sepB012001 = sepB012001;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dtecSeq != null ? dtecSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDirTecnico)) {
            return false;
        }
        TbDirTecnico other = (TbDirTecnico) object;
        if ((this.dtecSeq == null && other.dtecSeq != null) || (this.dtecSeq != null && !this.dtecSeq.equals(other.dtecSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbDirTecnico[ dtecSeq=" + dtecSeq + " ]";
    }
    
}
