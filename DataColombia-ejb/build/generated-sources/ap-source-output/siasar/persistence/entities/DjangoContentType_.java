package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DjangoContentType.class)
public abstract class DjangoContentType_ {

	public static volatile SingularAttribute<DjangoContentType, String> appLabel;
	public static volatile ListAttribute<DjangoContentType, AuthPermission> authPermissionList;
	public static volatile ListAttribute<DjangoContentType, DjangoAdminLog> djangoAdminLogList;
	public static volatile SingularAttribute<DjangoContentType, String> model;
	public static volatile SingularAttribute<DjangoContentType, Integer> id;

}

