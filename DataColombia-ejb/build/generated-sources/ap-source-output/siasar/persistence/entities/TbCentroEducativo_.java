package siasar.persistence.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbCentroEducativo.class)
public abstract class TbCentroEducativo_ {

	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC008001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC033001;
	public static volatile SingularAttribute<TbCentroEducativo, String> comC010001;
	public static volatile SingularAttribute<TbCentroEducativo, String> comC004001;
	public static volatile SingularAttribute<TbCentroEducativo, String> comC014001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC027001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC020001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC047001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC017001;
	public static volatile SingularAttribute<TbCentroEducativo, BigInteger> sisteSeq;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC043001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC024001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC036001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC007001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC032001;
	public static volatile SingularAttribute<TbCentroEducativo, Long> cenedSeq;
	public static volatile SingularAttribute<TbCentroEducativo, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC028001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC049001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC021001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC046001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC042001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC018001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC025001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC039001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC029001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC035001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC006001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC031001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC048001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC045001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC019001;
	public static volatile SingularAttribute<TbCentroEducativo, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC022001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC041001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC015001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC038001;
	public static volatile SingularAttribute<TbCentroEducativo, String> comC005001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC030001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC009001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC013001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC034001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC026001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC044001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC016001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC050001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC023001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC040001;
	public static volatile SingularAttribute<TbCentroEducativo, Integer> comC037001;

}

