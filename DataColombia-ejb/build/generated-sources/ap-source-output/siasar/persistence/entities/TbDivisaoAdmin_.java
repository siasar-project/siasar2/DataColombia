package siasar.persistence.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbDivisaoAdmin.class)
public abstract class TbDivisaoAdmin_ {

	public static volatile SingularAttribute<TbDivisaoAdmin, Date> dateLastUpdate;
	public static volatile ListAttribute<TbDivisaoAdmin, TbVariavelDw> tbVariavelDwList;
	public static volatile SingularAttribute<TbDivisaoAdmin, BigInteger> tdaPai;
	public static volatile SingularAttribute<TbDivisaoAdmin, String> tdaPais;
	public static volatile SingularAttribute<TbDivisaoAdmin, String> tdaNome;
	public static volatile SingularAttribute<TbDivisaoAdmin, Long> tdaSeq;

}

