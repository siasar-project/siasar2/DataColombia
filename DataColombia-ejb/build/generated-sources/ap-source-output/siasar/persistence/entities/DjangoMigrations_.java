package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DjangoMigrations.class)
public abstract class DjangoMigrations_ {

	public static volatile SingularAttribute<DjangoMigrations, String> app;
	public static volatile SingularAttribute<DjangoMigrations, Date> applied;
	public static volatile SingularAttribute<DjangoMigrations, String> name;
	public static volatile SingularAttribute<DjangoMigrations, Integer> id;

}

