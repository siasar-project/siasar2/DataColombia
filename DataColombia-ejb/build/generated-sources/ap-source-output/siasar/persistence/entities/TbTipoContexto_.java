package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTipoContexto.class)
public abstract class TbTipoContexto_ {

	public static volatile ListAttribute<TbTipoContexto, TbTipoContexto> tbTipoContextoList;
	public static volatile SingularAttribute<TbTipoContexto, Integer> tipoConSeq;
	public static volatile SingularAttribute<TbTipoContexto, String> tipoConNome;
	public static volatile ListAttribute<TbTipoContexto, TbVariavelDw> tbVariavelDwList;
	public static volatile SingularAttribute<TbTipoContexto, TbTipoContexto> tipoConSeqPai;
	public static volatile ListAttribute<TbTipoContexto, TbVariavel> tbVariavelList;
	public static volatile SingularAttribute<TbTipoContexto, String> tipoConCodigo;

}

