package siasar.persistence.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbComunidade.class)
public abstract class TbComunidade_ {

	public static volatile SingularAttribute<TbComunidade, Integer> comA016001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB011001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB005001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB009001;
	public static volatile ListAttribute<TbComunidade, TbCentroEducativo> tbCentroEducativoList;
	public static volatile SingularAttribute<TbComunidade, Boolean> comB021001;
	public static volatile SingularAttribute<TbComunidade, String> comF001001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comB002001;
	public static volatile SingularAttribute<TbComunidade, String> comA002001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comA025001;
	public static volatile SingularAttribute<TbComunidade, String> comA006001;
	public static volatile SingularAttribute<TbComunidade, BigInteger> patSeq;
	public static volatile SingularAttribute<TbComunidade, Integer> comB018001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comC001001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB014001;
	public static volatile SingularAttribute<TbComunidade, Integer> comA015001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB006001;
	public static volatile SingularAttribute<TbComunidade, Long> comunSeq;
	public static volatile SingularAttribute<TbComunidade, Integer> comA023001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB020001;
	public static volatile ListAttribute<TbComunidade, TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList;
	public static volatile SingularAttribute<TbComunidade, String> urlImagem;
	public static volatile SingularAttribute<TbComunidade, Integer> comA005001;
	public static volatile SingularAttribute<TbComunidade, Double> comA009001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comA026001;
	public static volatile SingularAttribute<TbComunidade, Integer> comA012001;
	public static volatile ListAttribute<TbComunidade, TbIntervNovoSistAgua> tbIntervNovoSistAguaList;
	public static volatile SingularAttribute<TbComunidade, Boolean> comD001001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB017001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB013001;
	public static volatile SingularAttribute<TbComunidade, Integer> comA014001;
	public static volatile ListAttribute<TbComunidade, TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList;
	public static volatile SingularAttribute<TbComunidade, Integer> comA018001;
	public static volatile ListAttribute<TbComunidade, TbGrpDomicilio> tbGrpDomicilioList;
	public static volatile SingularAttribute<TbComunidade, Date> comA001001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comB003001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB007001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB023001;
	public static volatile SingularAttribute<TbComunidade, Integer> comA004001;
	public static volatile SingularAttribute<TbComunidade, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbComunidade, Double> comA008001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comA027001;
	public static volatile SingularAttribute<TbComunidade, Integer> comC003001;
	public static volatile SingularAttribute<TbComunidade, String> classificacao;
	public static volatile SingularAttribute<TbComunidade, String> paisSigla;
	public static volatile SingularAttribute<TbComunidade, Integer> comA011001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB010001;
	public static volatile SingularAttribute<TbComunidade, String> comD002001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB016001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB012001;
	public static volatile SingularAttribute<TbComunidade, Integer> comA013001;
	public static volatile SingularAttribute<TbComunidade, String> comA017001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB022001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB004001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB008001;
	public static volatile ListAttribute<TbComunidade, TbIntervMelhoria> tbIntervMelhoriaList;
	public static volatile SingularAttribute<TbComunidade, String> comA028001;
	public static volatile SingularAttribute<TbComunidade, String> comA003001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comB001001;
	public static volatile SingularAttribute<TbComunidade, Double> comA007001;
	public static volatile SingularAttribute<TbComunidade, Boolean> comA024001;
	public static volatile ListAttribute<TbComunidade, TbCentroSaude> tbCentroSaudeList;
	public static volatile SingularAttribute<TbComunidade, Boolean> validado;
	public static volatile SingularAttribute<TbComunidade, String> comC002001;
	public static volatile SingularAttribute<TbComunidade, String> comA010001;
	public static volatile SingularAttribute<TbComunidade, Date> dataValidacao;
	public static volatile SingularAttribute<TbComunidade, Integer> comB015001;
	public static volatile SingularAttribute<TbComunidade, Integer> comB019001;
	public static volatile SingularAttribute<TbComunidade, String> comD003001;

}

