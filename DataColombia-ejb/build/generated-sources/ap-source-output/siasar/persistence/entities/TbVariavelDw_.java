package siasar.persistence.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbVariavelDw.class)
public abstract class TbVariavelDw_ {

	public static volatile SingularAttribute<TbVariavelDw, String> vardwErro;
	public static volatile SingularAttribute<TbVariavelDw, TbDivisaoAdmin> vardwLocalidade;
	public static volatile SingularAttribute<TbVariavelDw, TbVariavel> variaSeq;
	public static volatile SingularAttribute<TbVariavelDw, BigInteger> vardwEntidade;
	public static volatile SingularAttribute<TbVariavelDw, TbTipoContexto> tipoConSeq;
	public static volatile SingularAttribute<TbVariavelDw, Date> vardwTempo;
	public static volatile SingularAttribute<TbVariavelDw, Long> vardwSeq;
	public static volatile SingularAttribute<TbVariavelDw, Float> vardwValor;
	public static volatile SingularAttribute<TbVariavelDw, Date> dataCalculo;

}

