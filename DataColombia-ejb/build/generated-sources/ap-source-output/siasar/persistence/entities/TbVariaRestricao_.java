package siasar.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbVariaRestricao.class)
public abstract class TbVariaRestricao_ {

	public static volatile SingularAttribute<TbVariaRestricao, Boolean> varesAtivo;
	public static volatile SingularAttribute<TbVariaRestricao, TbVariavel> variaSeq;
	public static volatile SingularAttribute<TbVariaRestricao, BigDecimal> varesValor;
	public static volatile SingularAttribute<TbVariaRestricao, String> varesFormula;
	public static volatile SingularAttribute<TbVariaRestricao, String> varesErro;
	public static volatile SingularAttribute<TbVariaRestricao, Long> varesSeq;

}

