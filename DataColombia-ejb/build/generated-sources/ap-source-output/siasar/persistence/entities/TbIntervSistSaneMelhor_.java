package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbIntervSistSaneMelhor.class)
public abstract class TbIntervSistSaneMelhor_ {

	public static volatile SingularAttribute<TbIntervSistSaneMelhor, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbIntervSistSaneMelhor, String> intssmFonte;
	public static volatile SingularAttribute<TbIntervSistSaneMelhor, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbIntervSistSaneMelhor, Long> intssmSeq;
	public static volatile SingularAttribute<TbIntervSistSaneMelhor, TbTaxoTermo> taxtSeq;

}

