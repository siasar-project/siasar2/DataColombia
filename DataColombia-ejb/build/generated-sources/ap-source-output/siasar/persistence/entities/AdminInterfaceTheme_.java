package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdminInterfaceTheme.class)
public abstract class AdminInterfaceTheme_ {

	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssDeleteButtonTextColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> css;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssHeaderLinkHoverColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> logoColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> relatedModalActive;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> listFilterDropdown;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> title;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> logoVisible;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> relatedModalRoundedCorners;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> titleColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> relatedModalBackgroundOpacity;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssSaveButtonBackgroundColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssSaveButtonBackgroundHoverColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssModuleBackgroundColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> logo;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> cssModuleRoundedCorners;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssHeaderTextColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, Integer> id;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> favicon;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssGenericLinkHoverColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssHeaderBackgroundColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> active;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> titleVisible;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssModuleLinkColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssSaveButtonTextColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> env;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssModuleTextColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssModuleLinkHoverColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssDeleteButtonBackgroundColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssDeleteButtonBackgroundHoverColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> envVisible;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> relatedModalBackgroundColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssGenericLinkColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> name;
	public static volatile SingularAttribute<AdminInterfaceTheme, String> cssHeaderLinkColor;
	public static volatile SingularAttribute<AdminInterfaceTheme, Boolean> recentActionsVisible;

}

