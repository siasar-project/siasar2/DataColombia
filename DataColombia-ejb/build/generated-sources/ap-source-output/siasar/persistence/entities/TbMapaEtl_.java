package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbMapaEtl.class)
public abstract class TbMapaEtl_ {

	public static volatile SingularAttribute<TbMapaEtl, Long> mapetlSeq;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlArquivo;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlComando;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlImagem;
	public static volatile SingularAttribute<TbMapaEtl, Date> mapetlDatUltExecucao;
	public static volatile SingularAttribute<TbMapaEtl, Boolean> mapetlAtivo;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlNome;
	public static volatile SingularAttribute<TbMapaEtl, Boolean> mapetlInstalado;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlParametro;
	public static volatile SingularAttribute<TbMapaEtl, Integer> mapetlPid;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlLog;
	public static volatile SingularAttribute<TbMapaEtl, Date> mapetlDatCriacao;
	public static volatile SingularAttribute<TbMapaEtl, String> mapetlCron;

}

