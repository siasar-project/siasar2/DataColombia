package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTaxoTermo.class)
public abstract class TbTaxoTermo_ {

	public static volatile ListAttribute<TbTaxoTermo, TbDirRepresentante> tbDirRepresentanteList1;
	public static volatile SingularAttribute<TbTaxoTermo, String> taxtDescricao;
	public static volatile ListAttribute<TbTaxoTermo, TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList;
	public static volatile ListAttribute<TbTaxoTermo, TbDirRepresentante> tbDirRepresentanteList;
	public static volatile ListAttribute<TbTaxoTermo, TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList;
	public static volatile ListAttribute<TbTaxoTermo, TbDirTecnico> tbDirTecnicoList;
	public static volatile ListAttribute<TbTaxoTermo, TbTipSistAbastecimento> tbTipSistAbastecimentoList;
	public static volatile ListAttribute<TbTaxoTermo, TbIntervMelhoria> tbIntervMelhoriaList;
	public static volatile SingularAttribute<TbTaxoTermo, String> taxtNome;
	public static volatile SingularAttribute<TbTaxoTermo, Date> dateLastUpdate;
	public static volatile ListAttribute<TbTaxoTermo, TbDirTecnico> tbDirTecnicoList1;
	public static volatile SingularAttribute<TbTaxoTermo, String> taxtPais;
	public static volatile ListAttribute<TbTaxoTermo, TbIntervNovoSistAgua> tbIntervNovoSistAguaList;
	public static volatile SingularAttribute<TbTaxoTermo, TbTaxoVocab> taxvSeq;
	public static volatile SingularAttribute<TbTaxoTermo, Long> taxtSeq;

}

