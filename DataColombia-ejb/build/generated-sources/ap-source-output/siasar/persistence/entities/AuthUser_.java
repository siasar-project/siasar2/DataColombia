package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthUser.class)
public abstract class AuthUser_ {

	public static volatile SingularAttribute<AuthUser, Date> lastLogin;
	public static volatile SingularAttribute<AuthUser, String> lastName;
	public static volatile ListAttribute<AuthUser, ExplorerQuerylog> explorerQuerylogList;
	public static volatile SingularAttribute<AuthUser, Boolean> isStaff;
	public static volatile SingularAttribute<AuthUser, Date> dateJoined;
	public static volatile ListAttribute<AuthUser, DjangoAdminLog> djangoAdminLogList;
	public static volatile ListAttribute<AuthUser, AuthUserUserPermissions> authUserUserPermissionsList;
	public static volatile SingularAttribute<AuthUser, Boolean> isSuperuser;
	public static volatile SingularAttribute<AuthUser, Boolean> isActive;
	public static volatile ListAttribute<AuthUser, ExplorerQuery> explorerQueryList;
	public static volatile SingularAttribute<AuthUser, String> firstName;
	public static volatile SingularAttribute<AuthUser, String> password;
	public static volatile ListAttribute<AuthUser, AuthUserGroups> authUserGroupsList;
	public static volatile SingularAttribute<AuthUser, Integer> id;
	public static volatile SingularAttribute<AuthUser, String> email;
	public static volatile SingularAttribute<AuthUser, String> username;

}

