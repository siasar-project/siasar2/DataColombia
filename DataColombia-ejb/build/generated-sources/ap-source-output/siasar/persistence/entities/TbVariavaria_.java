package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbVariavaria.class)
public abstract class TbVariavaria_ {

	public static volatile SingularAttribute<TbVariavaria, Long> vavariaSeq;
	public static volatile SingularAttribute<TbVariavaria, TbVariavel> variaPai;
	public static volatile SingularAttribute<TbVariavaria, TbVariavel> variaFilho;

}

