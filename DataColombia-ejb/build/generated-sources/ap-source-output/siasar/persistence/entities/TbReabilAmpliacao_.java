package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbReabilAmpliacao.class)
public abstract class TbReabilAmpliacao_ {

	public static volatile SingularAttribute<TbReabilAmpliacao, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA021001;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA019001;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA020001;
	public static volatile SingularAttribute<TbReabilAmpliacao, BigDecimal> sysA026001;
	public static volatile SingularAttribute<TbReabilAmpliacao, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA022001;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA025001;
	public static volatile SingularAttribute<TbReabilAmpliacao, Integer> sysA023001;
	public static volatile SingularAttribute<TbReabilAmpliacao, BigDecimal> sysA024001;
	public static volatile SingularAttribute<TbReabilAmpliacao, Long> reampSeq;

}

