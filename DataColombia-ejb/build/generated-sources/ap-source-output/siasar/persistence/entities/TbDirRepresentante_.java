package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbDirRepresentante.class)
public abstract class TbDirRepresentante_ {

	public static volatile SingularAttribute<TbDirRepresentante, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbDirRepresentante, String> sepB009001;
	public static volatile SingularAttribute<TbDirRepresentante, TbPrestServico> prserSeq;
	public static volatile SingularAttribute<TbDirRepresentante, Long> drepSeq;
	public static volatile SingularAttribute<TbDirRepresentante, String> sepB007001;
	public static volatile SingularAttribute<TbDirRepresentante, TbTaxoTermo> sepB006001;
	public static volatile SingularAttribute<TbDirRepresentante, TbTaxoTermo> sepB008001;

}

