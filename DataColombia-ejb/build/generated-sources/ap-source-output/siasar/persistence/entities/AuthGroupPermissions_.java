package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthGroupPermissions.class)
public abstract class AuthGroupPermissions_ {

	public static volatile SingularAttribute<AuthGroupPermissions, AuthPermission> permissionId;
	public static volatile SingularAttribute<AuthGroupPermissions, AuthGroup> groupId;
	public static volatile SingularAttribute<AuthGroupPermissions, Integer> id;

}

