package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeographyColumns.class)
public abstract class GeographyColumns_ {

	public static volatile SingularAttribute<GeographyColumns, String> FTableCatalog;
	public static volatile SingularAttribute<GeographyColumns, Integer> coordDimension;
	public static volatile SingularAttribute<GeographyColumns, String> FTableSchema;
	public static volatile SingularAttribute<GeographyColumns, String> type;
	public static volatile SingularAttribute<GeographyColumns, String> FTableName;
	public static volatile SingularAttribute<GeographyColumns, String> FGeographyColumn;
	public static volatile SingularAttribute<GeographyColumns, Integer> srid;

}

