<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="es-es" />
        <title>SIASAR</title>
        <!--script type="text/javascript" src="<-%=request.getContextPath()%>/misc/js/jquery-1.8.0.min.js"></script-->
        <link href="<%=request.getContextPath()%>/resources/css/default.css" rel="stylesheet" type="text/css" />
    </head>
    
    <body>
        <div id="conte_index">
            <div id="escudo"><img src="<%=request.getContextPath()%>/resources/images/siasar_grande.png" width="680px"  /></div>
            <br/>
            <div id="col_der_index">
                <div id="bienvenido">Bienvenido</div>
                <div id="bienvenido_txt">Esta herramienta permite ....
                </div>

                <div id="contenido">
                    <div class="contenido_central">
                        <table class="caja_aviso">
                            <tbody>
                                <tr>
                                    <td><img src="<%=request.getContextPath()%>/resources/images/cargando.gif" alt="" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Iniciando sesi&oacute;n</h3>
                                        <p>Iniciando sesi&oacute;n y comprobando sus permisos en el sistema. Espere un momento, por favor...</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <form method="POST" name="login" action="j_security_check">
                        <input type="hidden" name="j_username" value="<%=request.getSession().getAttribute("login")%>"/>
                        <input type="hidden" name="j_password" value="default"/>
                    </form>
                </div> 
            </div>    
        </div>    
        <script type="text/javascript">
            document.forms["login"].submit();
        </script>
    </body>
</html>
